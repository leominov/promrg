package main

import (
	"os"
	"os/signal"
	"syscall"

	"github.com/sirupsen/logrus"
)

func main() {
	logrus.SetFormatter(&logrus.JSONFormatter{})

	logrus.Info("Starting promrg...")

	config, err := LoadFromEnv()
	if err != nil {
		logrus.WithError(err).Fatal("Failed to parse configuration")
	}
	logrus.SetLevel(config.LogLevel)

	if len(config.Targets) == 0 {
		logrus.Fatal("Empty targets list")
	}

	errChan := make(chan error)
	server := NewServer(config, errChan)

	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM)

	logrus.Infof("Listen address: %s", config.ListenAddress)

	go server.Run()

	for {
		select {
		case err := <-errChan:
			logrus.Fatal(err.Error())
		case s := <-signalChan:
			logrus.Infof("Captured %v. Exiting...", s)
			os.Exit(0)
		}
	}
}
