FROM golang:1.14-alpine3.11 as builder
WORKDIR /go/src/github.com/leominov/promrg
COPY . .
RUN go build ./

FROM alpine:3.11
RUN apk --no-cache add curl busybox-extras
COPY --from=builder /go/src/github.com/leominov/promrg/promrg /go/bin/promrg
ENTRYPOINT ["/go/bin/promrg"]
