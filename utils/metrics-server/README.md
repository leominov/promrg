1. Server

```shell script
PROFILE=1 TARGET_1=http://127.0.0.1:8081/a TARGET_2=http://127.0.0.1:8081/b go run ./
```
2. Generator

```shell script
go run ./utils/metrics-server
```

3. Result

```shell script
curl 127.0.0.1:8080/metrics
```
