package main

import (
	"flag"
	"fmt"
	"net/http"

	"github.com/sirupsen/logrus"
)

const (
	templateMeta = `# HELP metric_name_%[1]d Information about metric
# TYPE metric_name_%[1]d gauge
`
	templateMetric = `metric_name_%[1]d{foo="bar"} %[2]d
`
)

var (
	metricsNumber = flag.Uint("metrics", 1024, "Number of served metrics")
	valuesNumber  = flag.Uint("values", 256, "Number of values per metric")
	listenAddress = flag.String("listen-address", ":8081", "Address to serve metrics")
)

func main() {
	flag.Parse()

	logrus.Infof("Listen address: %s", *listenAddress)

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		for a := 1; a < int(*metricsNumber)-1; a++ {
			data := fmt.Sprintf(templateMeta, a)
			w.Write([]byte(data))
			for b := 1; b < int(*valuesNumber-1); b++ {
				data := fmt.Sprintf(templateMetric, a, b)
				w.Write([]byte(data))
			}
		}
	})

	logrus.Fatal(http.ListenAndServe(*listenAddress, nil))
}
