module github.com/leominov/promrg

go 1.14

require (
	github.com/gogo/protobuf v1.3.1
	github.com/hashicorp/go-cleanhttp v0.5.1
	github.com/prometheus/client_model v0.2.0
	github.com/prometheus/common v0.13.0
	github.com/sirupsen/logrus v1.6.0
	github.com/stretchr/testify v1.4.0
)
