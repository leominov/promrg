# promrg

[![Build Status](https://travis-ci.com/leominov/promrg.svg?branch=master)](https://travis-ci.com/leominov/promrg)
[![codecov](https://codecov.io/gh/leominov/promrg/branch/master/graph/badge.svg)](https://codecov.io/gh/leominov/promrg)

Based on https://github.com/rebuy-de/exporter-merger.

## Configuration

```
export HTTP_CLIENT_TIMEOUT=10s
export LOG_LEVEL=info
export PROFILE=true

export TELEMETRY_PATH=/metrics
export LISTEN_ADDRESS=0.0.0.0:8080

export TARGET_1=http://127.0.0.1:9080/metrics
export TARGET_1_LABELS='[{"name": "container_name", "value": "app1"}]'
export TARGET_2=http://127.0.0.1:9090/metrics
export TARGET_2_LABELS='[{"name": "container_name", "value": "app2"}]'
```

## Releases

- https://hub.docker.com/repository/docker/leominov/promrg
