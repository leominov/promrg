package main

import (
	"encoding/json"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	prom "github.com/prometheus/client_model/go"
	"github.com/sirupsen/logrus"
)

var (
	targetRegExp = regexp.MustCompile(`^TARGET_([\d])+$`)
)

type Config struct {
	HttpClientTimeout time.Duration
	LogLevel          logrus.Level
	ListenAddress     string
	TelemetryPath     string
	Targets           []*Target
	Profile           bool
}

type Target struct {
	Alias  string
	URL    string
	Labels []*prom.LabelPair
}

func ParseStringAsTarget(s string) *Target {
	data := strings.Split(s, "=")
	if len(data) < 2 || len(data[1]) == 0 {
		return nil
	}
	if !targetRegExp.MatchString(data[0]) {
		return nil
	}
	addr := data[1]
	if !strings.Contains(addr, "://") {
		addr = "http://" + addr
	}
	return &Target{
		Alias: data[0],
		URL:   addr,
	}
}

func ParseTargetsFromEnv() ([]*Target, error) {
	var targets []*Target

	for _, env := range os.Environ() {
		target := ParseStringAsTarget(env)
		if target == nil {
			continue
		}
		targets = append(targets, target)

		labelsRaw, ok := os.LookupEnv(target.Alias + "_LABELS")
		if !ok {
			continue
		}

		var labels []*prom.LabelPair
		err := json.Unmarshal([]byte(labelsRaw), &labels)
		if err != nil {
			return nil, fmt.Errorf("failed to parse %s labels: %v", target.Alias, err)
		}
		target.Labels = labels
	}

	return targets, nil
}

func LoadFromEnv() (*Config, error) {
	c := &Config{
		LogLevel:      logrus.InfoLevel,
		ListenAddress: "0.0.0.0:8080",
		TelemetryPath: "/metrics",
		Profile:       false,
	}
	if httpClientTimeoutRaw := os.Getenv("HTTP_CLIENT_TIMEOUT"); len(httpClientTimeoutRaw) > 0 {
		httpClientTimeout, err := time.ParseDuration(httpClientTimeoutRaw)
		if err != nil {
			return nil, fmt.Errorf("failed to parse HTTP_CLIENT_TIMEOUT: %v", err)
		}
		c.HttpClientTimeout = httpClientTimeout
	}
	if telemetryPath := os.Getenv("TELEMETRY_PATH"); len(telemetryPath) > 0 {
		c.TelemetryPath = telemetryPath
	}
	if listenAddress := os.Getenv("LISTEN_ADDRESS"); len(listenAddress) > 0 {
		c.ListenAddress = listenAddress
	}
	if profileRaw := os.Getenv("PROFILE"); len(profileRaw) > 0 {
		v, _ := strconv.ParseBool(profileRaw)
		c.Profile = v
	}
	if levelRaw := os.Getenv("LOG_LEVEL"); len(levelRaw) > 0 {
		level, err := logrus.ParseLevel(levelRaw)
		if err != nil {
			return nil, err
		}
		c.LogLevel = level
	}
	targets, err := ParseTargetsFromEnv()
	c.Targets = targets
	return c, err
}
