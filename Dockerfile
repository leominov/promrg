FROM golang:1.14-alpine3.11 as builder
WORKDIR /go/src/github.com/leominov/promrg
COPY . .
RUN apk --no-cache add upx && \
    CGO_ENABLED=0 go build -ldflags '-w' ./ && \
    upx -9 promrg

FROM scratch
COPY --from=builder /go/src/github.com/leominov/promrg/promrg /go/bin/promrg
ENTRYPOINT ["/go/bin/promrg"]
