package main

import (
	"io"
	"net/http"
	"net/http/pprof"
	_ "net/http/pprof"
	"sort"
	"sync"

	"github.com/hashicorp/go-cleanhttp"
	prom "github.com/prometheus/client_model/go"
	"github.com/prometheus/common/expfmt"
	"github.com/sirupsen/logrus"
)

type Server struct {
	cli     *http.Client
	mux     *http.ServeMux
	c       *Config
	errChan chan error
}

func NewServer(c *Config, errChan chan error) *Server {
	s := &Server{
		mux:     http.NewServeMux(),
		cli:     cleanhttp.DefaultClient(),
		c:       c,
		errChan: errChan,
	}
	s.cli.Timeout = c.HttpClientTimeout
	s.addHandlers()
	return s
}

func HealthHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
}

func (s *Server) addHandlers() {
	s.mux.HandleFunc("/health", HealthHandler)

	if s.c.Profile {
		s.mux.HandleFunc("/debug/pprof/", pprof.Index)
		s.mux.HandleFunc("/debug/pprof/cmdline", pprof.Cmdline)
		s.mux.HandleFunc("/debug/pprof/profile", pprof.Profile)
		s.mux.HandleFunc("/debug/pprof/symbol", pprof.Symbol)
		s.mux.HandleFunc("/debug/pprof/trace", pprof.Trace)
	}

	s.mux.HandleFunc(s.c.TelemetryPath, s.ServeHTTP)
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	logrus.WithFields(logrus.Fields{
		"request_uri": r.RequestURI,
		"user_agent":  r.UserAgent(),
	}).Debug("Handling metrics request")

	s.Merge(w)
}

func (s *Server) fetchMetrics(link string) (map[string]*prom.MetricFamily, error) {
	resp, err := s.cli.Get(link)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	tp := new(expfmt.TextParser)
	return tp.TextToMetricFamilies(resp.Body)
}

func enrichMetricFamilyByTarget(mf *prom.MetricFamily, t *Target) {
	for _, metric := range mf.Metric {
		metric.Label = append(metric.Label, t.Labels...)
		sort.Sort(labelPairSorter(metric.Label))
	}
}

func concatResponses(responses []map[string]*prom.MetricFamily) map[string]*prom.MetricFamily {
	mfs := map[string]*prom.MetricFamily{}
	for _, part := range responses {
		for n, mf := range part {
			if mfo, ok := mfs[n]; ok {
				mfo.Metric = append(mfo.Metric, mf.Metric...)
				continue
			}
			mfs[n] = mf
		}
	}
	return mfs
}

func (s *Server) Merge(w io.Writer) {
	var names []string

	wg := sync.WaitGroup{}

	responses := make([]map[string]*prom.MetricFamily, 1024)
	responsesMu := sync.Mutex{}

	for _, target := range s.c.Targets {
		wg.Add(1)
		go func(t *Target) {
			defer wg.Done()

			logrus.WithField("target", t.URL).Debug("Requesting metrics")

			response, err := s.fetchMetrics(t.URL)
			if err != nil {
				logrus.WithField("target", t.URL).WithError(err).Error("Failed to fetch")
				return
			}

			for _, family := range response {
				enrichMetricFamilyByTarget(family, t)
			}

			responsesMu.Lock()
			responses = append(responses, response)
			responsesMu.Unlock()
		}(target)
	}
	wg.Wait()

	mfs := concatResponses(responses)
	for n := range mfs {
		names = append(names, n)
	}

	sort.Strings(names)

	enc := expfmt.NewEncoder(w, expfmt.FmtText)
	for _, n := range names {
		if err := enc.Encode(mfs[n]); err != nil {
			logrus.Error(err)
		}
	}
}

func (s *Server) Run() {
	s.errChan <- http.ListenAndServe(s.c.ListenAddress, s.mux)
}
