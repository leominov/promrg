package main

import (
	"os"
	"testing"
	"time"

	"github.com/gogo/protobuf/proto"
	prom "github.com/prometheus/client_model/go"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

func TestParseTargetsFromEnv(t *testing.T) {
	os.Unsetenv("TARGET_1")
	targets, err := ParseTargetsFromEnv()
	assert.NoError(t, err)
	assert.Equal(t, []*Target(nil), targets)

	os.Setenv("TARGET_1", "127.0.0.1:9910/metric")
	os.Setenv("TARGET_2", "")
	targets, err = ParseTargetsFromEnv()
	assert.NoError(t, err)
	assert.Equal(t, []*Target{
		{
			Alias: "TARGET_1",
			URL:   "http://127.0.0.1:9910/metric",
		},
	}, targets)

	os.Setenv("TARGET_1_LABELS", `{"a`)
	_, err = ParseTargetsFromEnv()
	assert.Error(t, err)

	os.Setenv("TARGET_1_LABELS", `[{"name": "foo", "value": "bar"}]`)
	targets, err = ParseTargetsFromEnv()
	assert.NoError(t, err)
	assert.Equal(t, []*Target{
		{
			Alias: "TARGET_1",
			URL:   "http://127.0.0.1:9910/metric",
			Labels: []*prom.LabelPair{
				{
					Name:  proto.String("foo"),
					Value: proto.String("bar"),
				},
			},
		},
	}, targets)

	os.Setenv("TARGET_2_LABELS", `[{"name": "aaa", "value": "bbb"}]`)
	targets, err = ParseTargetsFromEnv()
	assert.NoError(t, err)
	assert.Equal(t, []*Target{
		{
			Alias: "TARGET_1",
			URL:   "http://127.0.0.1:9910/metric",
			Labels: []*prom.LabelPair{
				{
					Name:  proto.String("foo"),
					Value: proto.String("bar"),
				},
			},
		},
	}, targets)

	os.Setenv("TARGET_2", "http://127.0.0.1:8080/metric")
	targets, err = ParseTargetsFromEnv()
	assert.NoError(t, err)
	assert.Equal(t, []*Target{
		{
			Alias: "TARGET_1",
			URL:   "http://127.0.0.1:9910/metric",
			Labels: []*prom.LabelPair{
				{
					Name:  proto.String("foo"),
					Value: proto.String("bar"),
				},
			},
		},
		{
			Alias: "TARGET_2",
			URL:   "http://127.0.0.1:8080/metric",
			Labels: []*prom.LabelPair{
				{
					Name:  proto.String("aaa"),
					Value: proto.String("bbb"),
				},
			},
		},
	}, targets)
}

func TestLoadFromEnv(t *testing.T) {
	for _, env := range []string{
		"HTTP_CLIENT_TIMEOUT",
		"TELEMETRY_PATH",
		"LISTEN_ADDRESS",
		"LOG_LEVEL",
	} {
		os.Unsetenv(env)
	}

	cfg, err := LoadFromEnv()
	assert.NoError(t, err)

	os.Setenv("HTTP_CLIENT_TIMEOUT", "ABCD")
	cfg, err = LoadFromEnv()
	assert.Error(t, err)

	os.Setenv("HTTP_CLIENT_TIMEOUT", "10s")
	cfg, err = LoadFromEnv()
	assert.NoError(t, err)
	assert.Equal(t, 10*time.Second, cfg.HttpClientTimeout)

	os.Setenv("LOG_LEVEL", "ABCD")
	cfg, err = LoadFromEnv()
	assert.Error(t, err)

	os.Setenv("LOG_LEVEL", "debug")
	cfg, err = LoadFromEnv()
	assert.NoError(t, err)
	assert.Equal(t, logrus.DebugLevel, cfg.LogLevel)

	os.Setenv("LISTEN_ADDRESS", ":9090")
	cfg, err = LoadFromEnv()
	assert.NoError(t, err)
	assert.Equal(t, ":9090", cfg.ListenAddress)

	os.Setenv("TELEMETRY_PATH", "/foobar")
	cfg, err = LoadFromEnv()
	assert.NoError(t, err)
	assert.Equal(t, "/foobar", cfg.TelemetryPath)
}
