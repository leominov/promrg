package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"sort"
	"strings"
	"testing"
	"time"

	"github.com/gogo/protobuf/proto"
	"github.com/hashicorp/go-cleanhttp"
	prom "github.com/prometheus/client_model/go"
	"github.com/stretchr/testify/assert"
)

func TestNewServer(t *testing.T) {
	errChan := make(chan error)
	s := NewServer(&Config{
		TelemetryPath:     "/metrics",
		HttpClientTimeout: time.Second,
		ListenAddress:     ":999999",
	}, errChan)
	go s.Run()
	err := <-errChan
	assert.Error(t, err)
}

func TestServer_Merge_1(t *testing.T) {
	ts1 := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, `aaa 1
bbb{foo="bar"} 2
ccc 3`)
	}))
	defer ts1.Close()

	ts2 := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, `aaa 4
bbb{foo="bar"} 5
ccc 6`)
	}))
	defer ts2.Close()

	config := &Config{
		Targets: []*Target{
			{
				URL: ts1.URL,
			},
			{
				URL: ts2.URL,
			},
		},
	}

	server := &Server{
		cli: cleanhttp.DefaultClient(),
		c:   config,
	}

	srv := httptest.NewServer(server)
	defer srv.Close()

	resp, err := http.Get(srv.URL)
	if !assert.NoError(t, err) {
		return
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if !assert.NoError(t, err) {
		return
	}

	s1 := strings.Split(`# TYPE aaa untyped
aaa 1
aaa 4
# TYPE bbb untyped
bbb{foo="bar"} 2
bbb{foo="bar"} 5
# TYPE ccc untyped
ccc 3
ccc 6
`, "\n")
	sort.Strings(s1)
	s2 := strings.Split(string(body), "\n")
	sort.Strings(s2)

	assert.Equal(t, s1, s2)
}

func TestServer_Merge_2(t *testing.T) {
	ts1 := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, `aaa 1
bbb{foo="bar"} 2
ccc 3`)
	}))
	defer ts1.Close()

	ts2 := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, `aaa 4
bbb{foo="bar"} 5
ccc 6`)
	}))
	defer ts2.Close()

	config := &Config{
		Targets: []*Target{
			{
				URL: ts1.URL,
				Labels: []*prom.LabelPair{
					{
						Name:  proto.String("aaa"),
						Value: proto.String("bbb"),
					},
				},
			},
			{
				URL: ts2.URL,
			},
		},
	}

	server := &Server{
		cli: cleanhttp.DefaultClient(),
		c:   config,
	}

	srv := httptest.NewServer(server)
	defer srv.Close()

	resp, err := http.Get(srv.URL)
	if !assert.NoError(t, err) {
		return
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if !assert.NoError(t, err) {
		return
	}

	s1 := strings.Split(`# TYPE aaa untyped
aaa{aaa="bbb"} 1
aaa 4
# TYPE bbb untyped
bbb{aaa="bbb",foo="bar"} 2
bbb{foo="bar"} 5
# TYPE ccc untyped
ccc{aaa="bbb"} 3
ccc 6
`, "\n")
	sort.Strings(s1)
	s2 := strings.Split(string(body), "\n")
	sort.Strings(s2)

	assert.Equal(t, s1, s2)
}

func TestServer_Merge_3(t *testing.T) {
	ts1 := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, `aaa 1
bbb{foo="bar"} 2
ccc 3`)
	}))
	defer ts1.Close()

	ts2 := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, `aaa 4
bbb{foo="bar"} 5
ccc 6`)
	}))
	defer ts2.Close()

	config := &Config{
		Targets: []*Target{
			{
				URL: ts1.URL,
				Labels: []*prom.LabelPair{
					{
						Name:  proto.String("aaa"),
						Value: proto.String("bbb"),
					},
				},
			},
			{
				URL: ts2.URL,
				Labels: []*prom.LabelPair{
					{
						Name:  proto.String("ccc"),
						Value: proto.String("ddd"),
					},
					{
						Name:  proto.String("yyy"),
						Value: proto.String("zzz"),
					},
				},
			},
			{
				URL: "abcd",
			},
		},
	}

	server := &Server{
		cli: cleanhttp.DefaultClient(),
		c:   config,
	}

	srv := httptest.NewServer(server)
	defer srv.Close()

	resp, err := http.Get(srv.URL)
	if !assert.NoError(t, err) {
		return
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if !assert.NoError(t, err) {
		return
	}

	s1 := strings.Split(`# TYPE aaa untyped
aaa{aaa="bbb"} 1
aaa{ccc="ddd",yyy="zzz"} 4
# TYPE bbb untyped
bbb{aaa="bbb",foo="bar"} 2
bbb{ccc="ddd",foo="bar",yyy="zzz"} 5
# TYPE ccc untyped
ccc{aaa="bbb"} 3
ccc{ccc="ddd",yyy="zzz"} 6
`, "\n")
	sort.Strings(s1)
	s2 := strings.Split(string(body), "\n")
	sort.Strings(s2)

	assert.Equal(t, s1, s2)
}

func TestHealthHandler(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(HealthHandler))
	defer ts.Close()
	resp, err := http.Get(ts.URL)
	if assert.NoError(t, err) {
		return
	}
	defer resp.Body.Close()
	assert.Equal(t, http.StatusOK, resp.StatusCode)
}
